FROM openjdk:8

ENV SBT_VERSION=1.2.8

RUN apt-get update -yqq && \
    apt-get install apt-transport-https -yqq && \
    echo "deb http://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list && \
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823 && \
    apt-get update -yqq && \
    apt-get install sbt=$SBT_VERSION -yqq && \
    sbt sbtVersion

